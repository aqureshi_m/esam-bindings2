//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.06.26 at 01:49:24 PM PDT 
//


package cablelabs.md.xsd.signaling._3;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the cablelabs.md.xsd.signaling._3 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AcquiredSignal_QNAME = new QName("urn:cablelabs:md:xsd:signaling:3.0", "AcquiredSignal");
    private final static QName _SignalGroupAsset_QNAME = new QName("urn:cablelabs:md:xsd:signaling:3.0", "SignalGroupAsset");
    private final static QName _SignalGroup_QNAME = new QName("urn:cablelabs:md:xsd:signaling:3.0", "SignalGroup");
    private final static QName _SignalRegion_QNAME = new QName("urn:cablelabs:md:xsd:signaling:3.0", "SignalRegion");
    private final static QName _SignalPoint_QNAME = new QName("urn:cablelabs:md:xsd:signaling:3.0", "SignalPoint");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: cablelabs.md.xsd.signaling._3
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SignaledPointInfoType }
     * 
     */
    public SignaledPointInfoType createSignaledPointInfoType() {
        return new SignaledPointInfoType();
    }

    /**
     * Create an instance of {@link SignalGroupAssetType }
     * 
     */
    public SignalGroupAssetType createSignalGroupAssetType() {
        return new SignalGroupAssetType();
    }

    /**
     * Create an instance of {@link SignalGroupType }
     * 
     */
    public SignalGroupType createSignalGroupType() {
        return new SignalGroupType();
    }

    /**
     * Create an instance of {@link AcquisitionPointInfoType }
     * 
     */
    public AcquisitionPointInfoType createAcquisitionPointInfoType() {
        return new AcquisitionPointInfoType();
    }

    /**
     * Create an instance of {@link SignalRegionType }
     * 
     */
    public SignalRegionType createSignalRegionType() {
        return new SignalRegionType();
    }

    /**
     * Create an instance of {@link SignalPointType }
     * 
     */
    public SignalPointType createSignalPointType() {
        return new SignalPointType();
    }

    /**
     * Create an instance of {@link SignalValidityTimeRangeType }
     * 
     */
    public SignalValidityTimeRangeType createSignalValidityTimeRangeType() {
        return new SignalValidityTimeRangeType();
    }

    /**
     * Create an instance of {@link BinarySignalType }
     * 
     */
    public BinarySignalType createBinarySignalType() {
        return new BinarySignalType();
    }

    /**
     * Create an instance of {@link StreamTimesType }
     * 
     */
    public StreamTimesType createStreamTimesType() {
        return new StreamTimesType();
    }

    /**
     * Create an instance of {@link UTCPointDescriptorType }
     * 
     */
    public UTCPointDescriptorType createUTCPointDescriptorType() {
        return new UTCPointDescriptorType();
    }

    /**
     * Create an instance of {@link StreamTimeType }
     * 
     */
    public StreamTimeType createStreamTimeType() {
        return new StreamTimeType();
    }

    /**
     * Create an instance of {@link NPTPointDescriptorType }
     * 
     */
    public NPTPointDescriptorType createNPTPointDescriptorType() {
        return new NPTPointDescriptorType();
    }

    /**
     * Create an instance of {@link SignaledPointInfoType.SignalValidityTimeRange }
     * 
     */
    public SignaledPointInfoType.SignalValidityTimeRange createSignaledPointInfoTypeSignalValidityTimeRange() {
        return new SignaledPointInfoType.SignalValidityTimeRange();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AcquisitionPointInfoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:cablelabs:md:xsd:signaling:3.0", name = "AcquiredSignal")
    public JAXBElement<AcquisitionPointInfoType> createAcquiredSignal(AcquisitionPointInfoType value) {
        return new JAXBElement<AcquisitionPointInfoType>(_AcquiredSignal_QNAME, AcquisitionPointInfoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignalGroupAssetType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:cablelabs:md:xsd:signaling:3.0", name = "SignalGroupAsset")
    public JAXBElement<SignalGroupAssetType> createSignalGroupAsset(SignalGroupAssetType value) {
        return new JAXBElement<SignalGroupAssetType>(_SignalGroupAsset_QNAME, SignalGroupAssetType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignalGroupType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:cablelabs:md:xsd:signaling:3.0", name = "SignalGroup")
    public JAXBElement<SignalGroupType> createSignalGroup(SignalGroupType value) {
        return new JAXBElement<SignalGroupType>(_SignalGroup_QNAME, SignalGroupType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignalRegionType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:cablelabs:md:xsd:signaling:3.0", name = "SignalRegion")
    public JAXBElement<SignalRegionType> createSignalRegion(SignalRegionType value) {
        return new JAXBElement<SignalRegionType>(_SignalRegion_QNAME, SignalRegionType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SignalPointType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:cablelabs:md:xsd:signaling:3.0", name = "SignalPoint")
    public JAXBElement<SignalPointType> createSignalPoint(SignalPointType value) {
        return new JAXBElement<SignalPointType>(_SignalPoint_QNAME, SignalPointType.class, null, value);
    }

}
