//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.06.26 at 01:49:24 PM PDT 
//


package cablelabs.md.xsd.signaling._3;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import cablelabs.md.xsd.core._3.AssetType;


/**
 * SignalGroupAssetType is an extension of the CableLabs 3 AssetType.  Implementers which do not deal with CableLabs Assets may safely ignore this type.
 * 
 * <p>Java class for SignalGroupAssetType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SignalGroupAssetType">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:cablelabs:md:xsd:core:3.0}AssetType">
 *       &lt;sequence>
 *         &lt;element name="SignalPoint" type="{urn:cablelabs:md:xsd:signaling:3.0}SignalPointType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SignalRegion" type="{urn:cablelabs:md:xsd:signaling:3.0}SignalRegionType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;anyAttribute processContents='lax'/>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SignalGroupAssetType", propOrder = {
    "signalPoint",
    "signalRegion"
})
public class SignalGroupAssetType
    extends AssetType
{

    @XmlElement(name = "SignalPoint")
    protected List<SignalPointType> signalPoint;
    @XmlElement(name = "SignalRegion")
    protected List<SignalRegionType> signalRegion;

    /**
     * Gets the value of the signalPoint property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the signalPoint property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSignalPoint().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SignalPointType }
     * 
     * 
     */
    public List<SignalPointType> getSignalPoint() {
        if (signalPoint == null) {
            signalPoint = new ArrayList<SignalPointType>();
        }
        return this.signalPoint;
    }

    /**
     * Gets the value of the signalRegion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the signalRegion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSignalRegion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SignalRegionType }
     * 
     * 
     */
    public List<SignalRegionType> getSignalRegion() {
        if (signalRegion == null) {
            signalRegion = new ArrayList<SignalRegionType>();
        }
        return this.signalRegion;
    }

}
