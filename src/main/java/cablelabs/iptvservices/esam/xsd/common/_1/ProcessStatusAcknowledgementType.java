//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.06.26 at 01:49:24 PM PDT 
//


package cablelabs.iptvservices.esam.xsd.common._1;

import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.namespace.QName;
import cablelabs.md.xsd.core._3.StatusCodeType;


/**
 * <p>Java class for ProcessStatusAcknowledgementType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProcessStatusAcknowledgementType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StatusCode" type="{urn:cablelabs:md:xsd:core:3.0}StatusCodeType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="acquisitionPointIdentity" type="{urn:cablelabs:md:xsd:core:3.0}NonEmptyStringType" />
 *       &lt;attribute name="acquisitionSignalID" type="{urn:cablelabs:md:xsd:signaling:3.0}AcquisitionSignalIDType" />
 *       &lt;attribute name="batchId" type="{urn:cablelabs:iptvservices:esam:xsd:common:1}BatchIDType" />
 *       &lt;attribute name="spsToken" type="{http://www.w3.org/2001/XMLSchema}token" />
 *       &lt;attribute name="sasToken" type="{http://www.w3.org/2001/XMLSchema}token" />
 *       &lt;anyAttribute processContents='lax'/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProcessStatusAcknowledgementType", propOrder = {
    "statusCode"
})
public class ProcessStatusAcknowledgementType {

    @XmlElement(name = "StatusCode")
    protected StatusCodeType statusCode;
    @XmlAttribute(name = "acquisitionPointIdentity")
    protected String acquisitionPointIdentity;
    @XmlAttribute(name = "acquisitionSignalID")
    protected String acquisitionSignalID;
    @XmlAttribute(name = "batchId")
    protected String batchId;
    @XmlAttribute(name = "spsToken")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String spsToken;
    @XmlAttribute(name = "sasToken")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String sasToken;
    @XmlAnyAttribute
    private Map<QName, String> otherAttributes = new HashMap<QName, String>();

    /**
     * Gets the value of the statusCode property.
     * 
     * @return
     *     possible object is
     *     {@link StatusCodeType }
     *     
     */
    public StatusCodeType getStatusCode() {
        return statusCode;
    }

    /**
     * Sets the value of the statusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusCodeType }
     *     
     */
    public void setStatusCode(StatusCodeType value) {
        this.statusCode = value;
    }

    /**
     * Gets the value of the acquisitionPointIdentity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcquisitionPointIdentity() {
        return acquisitionPointIdentity;
    }

    /**
     * Sets the value of the acquisitionPointIdentity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcquisitionPointIdentity(String value) {
        this.acquisitionPointIdentity = value;
    }

    /**
     * Gets the value of the acquisitionSignalID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcquisitionSignalID() {
        return acquisitionSignalID;
    }

    /**
     * Sets the value of the acquisitionSignalID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcquisitionSignalID(String value) {
        this.acquisitionSignalID = value;
    }

    /**
     * Gets the value of the batchId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBatchId() {
        return batchId;
    }

    /**
     * Sets the value of the batchId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBatchId(String value) {
        this.batchId = value;
    }

    /**
     * Gets the value of the spsToken property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpsToken() {
        return spsToken;
    }

    /**
     * Sets the value of the spsToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpsToken(String value) {
        this.spsToken = value;
    }

    /**
     * Gets the value of the sasToken property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSasToken() {
        return sasToken;
    }

    /**
     * Sets the value of the sasToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSasToken(String value) {
        this.sasToken = value;
    }

    /**
     * Gets a map that contains attributes that aren't bound to any typed property on this class.
     * 
     * <p>
     * the map is keyed by the name of the attribute and 
     * the value is the string value of the attribute.
     * 
     * the map returned by this method is live, and you can add new attribute
     * by updating the map directly. Because of this design, there's no setter.
     * 
     * 
     * @return
     *     always non-null
     */
    public Map<QName, String> getOtherAttributes() {
        return otherAttributes;
    }

}
