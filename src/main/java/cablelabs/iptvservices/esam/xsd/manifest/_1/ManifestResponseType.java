//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.06.26 at 01:49:24 PM PDT 
//


package cablelabs.iptvservices.esam.xsd.manifest._1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.datatype.Duration;
import javax.xml.namespace.QName;


/**
 * <p>Java class for ManifestResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ManifestResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SegmentModify" type="{urn:cablelabs:iptvservices:esam:xsd:manifest:1}SegmentModifyType" minOccurs="0"/>
 *         &lt;element name="SegmentReplace" type="{urn:cablelabs:iptvservices:esam:xsd:manifest:1}SegmentReplaceType" minOccurs="0"/>
 *         &lt;element name="SparseTrack" type="{urn:cablelabs:iptvservices:esam:xsd:manifest:1}SparseTrackType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SpliceCueInfo" type="{urn:cablelabs:iptvservices:esam:xsd:manifest:1}SpliceCueInfoType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SecurityMetadata" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="TemplateResponse" type="{urn:cablelabs:iptvservices:esam:xsd:manifest:1}TemplateResponseType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="acquisitionPointIdentity" type="{urn:cablelabs:md:xsd:core:3.0}NonEmptyStringType" />
 *       &lt;attribute name="acquisitionSignalID" type="{urn:cablelabs:md:xsd:signaling:3.0}AcquisitionSignalIDType" />
 *       &lt;attribute name="spsToken" type="{http://www.w3.org/2001/XMLSchema}token" />
 *       &lt;attribute name="sasToken" type="{http://www.w3.org/2001/XMLSchema}token" />
 *       &lt;attribute name="signalPointID" type="{urn:cablelabs:md:xsd:core:3.0}URIIdType" />
 *       &lt;attribute name="duration" type="{http://www.w3.org/2001/XMLSchema}duration" />
 *       &lt;attribute name="dataPassThrough" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;anyAttribute processContents='lax'/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ManifestResponseType", propOrder = {
    "segmentModify",
    "segmentReplace",
    "sparseTrack",
    "spliceCueInfo",
    "securityMetadata",
    "templateResponse"
})
public class ManifestResponseType {

    @XmlElement(name = "SegmentModify")
    protected SegmentModifyType segmentModify;
    @XmlElement(name = "SegmentReplace")
    protected SegmentReplaceType segmentReplace;
    @XmlElement(name = "SparseTrack")
    protected List<SparseTrackType> sparseTrack;
    @XmlElement(name = "SpliceCueInfo")
    protected List<SpliceCueInfoType> spliceCueInfo;
    @XmlElement(name = "SecurityMetadata")
    protected Object securityMetadata;
    @XmlElement(name = "TemplateResponse")
    protected TemplateResponseType templateResponse;
    @XmlAttribute(name = "acquisitionPointIdentity")
    protected String acquisitionPointIdentity;
    @XmlAttribute(name = "acquisitionSignalID")
    protected String acquisitionSignalID;
    @XmlAttribute(name = "spsToken")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String spsToken;
    @XmlAttribute(name = "sasToken")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String sasToken;
    @XmlAttribute(name = "signalPointID")
    protected String signalPointID;
    @XmlAttribute(name = "duration")
    protected Duration duration;
    @XmlAttribute(name = "dataPassThrough")
    protected Boolean dataPassThrough;
    @XmlAnyAttribute
    private Map<QName, String> otherAttributes = new HashMap<QName, String>();

    /**
     * Gets the value of the segmentModify property.
     * 
     * @return
     *     possible object is
     *     {@link SegmentModifyType }
     *     
     */
    public SegmentModifyType getSegmentModify() {
        return segmentModify;
    }

    /**
     * Sets the value of the segmentModify property.
     * 
     * @param value
     *     allowed object is
     *     {@link SegmentModifyType }
     *     
     */
    public void setSegmentModify(SegmentModifyType value) {
        this.segmentModify = value;
    }

    /**
     * Gets the value of the segmentReplace property.
     * 
     * @return
     *     possible object is
     *     {@link SegmentReplaceType }
     *     
     */
    public SegmentReplaceType getSegmentReplace() {
        return segmentReplace;
    }

    /**
     * Sets the value of the segmentReplace property.
     * 
     * @param value
     *     allowed object is
     *     {@link SegmentReplaceType }
     *     
     */
    public void setSegmentReplace(SegmentReplaceType value) {
        this.segmentReplace = value;
    }

    /**
     * Gets the value of the sparseTrack property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sparseTrack property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSparseTrack().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SparseTrackType }
     * 
     * 
     */
    public List<SparseTrackType> getSparseTrack() {
        if (sparseTrack == null) {
            sparseTrack = new ArrayList<SparseTrackType>();
        }
        return this.sparseTrack;
    }

    /**
     * Gets the value of the spliceCueInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the spliceCueInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpliceCueInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SpliceCueInfoType }
     * 
     * 
     */
    public List<SpliceCueInfoType> getSpliceCueInfo() {
        if (spliceCueInfo == null) {
            spliceCueInfo = new ArrayList<SpliceCueInfoType>();
        }
        return this.spliceCueInfo;
    }

    /**
     * Gets the value of the securityMetadata property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getSecurityMetadata() {
        return securityMetadata;
    }

    /**
     * Sets the value of the securityMetadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setSecurityMetadata(Object value) {
        this.securityMetadata = value;
    }

    /**
     * Gets the value of the templateResponse property.
     * 
     * @return
     *     possible object is
     *     {@link TemplateResponseType }
     *     
     */
    public TemplateResponseType getTemplateResponse() {
        return templateResponse;
    }

    /**
     * Sets the value of the templateResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link TemplateResponseType }
     *     
     */
    public void setTemplateResponse(TemplateResponseType value) {
        this.templateResponse = value;
    }

    /**
     * Gets the value of the acquisitionPointIdentity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcquisitionPointIdentity() {
        return acquisitionPointIdentity;
    }

    /**
     * Sets the value of the acquisitionPointIdentity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcquisitionPointIdentity(String value) {
        this.acquisitionPointIdentity = value;
    }

    /**
     * Gets the value of the acquisitionSignalID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcquisitionSignalID() {
        return acquisitionSignalID;
    }

    /**
     * Sets the value of the acquisitionSignalID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcquisitionSignalID(String value) {
        this.acquisitionSignalID = value;
    }

    /**
     * Gets the value of the spsToken property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpsToken() {
        return spsToken;
    }

    /**
     * Sets the value of the spsToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpsToken(String value) {
        this.spsToken = value;
    }

    /**
     * Gets the value of the sasToken property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSasToken() {
        return sasToken;
    }

    /**
     * Sets the value of the sasToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSasToken(String value) {
        this.sasToken = value;
    }

    /**
     * Gets the value of the signalPointID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSignalPointID() {
        return signalPointID;
    }

    /**
     * Sets the value of the signalPointID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSignalPointID(String value) {
        this.signalPointID = value;
    }

    /**
     * Gets the value of the duration property.
     * 
     * @return
     *     possible object is
     *     {@link Duration }
     *     
     */
    public Duration getDuration() {
        return duration;
    }

    /**
     * Sets the value of the duration property.
     * 
     * @param value
     *     allowed object is
     *     {@link Duration }
     *     
     */
    public void setDuration(Duration value) {
        this.duration = value;
    }

    /**
     * Gets the value of the dataPassThrough property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDataPassThrough() {
        return dataPassThrough;
    }

    /**
     * Sets the value of the dataPassThrough property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDataPassThrough(Boolean value) {
        this.dataPassThrough = value;
    }

    /**
     * Gets a map that contains attributes that aren't bound to any typed property on this class.
     * 
     * <p>
     * the map is keyed by the name of the attribute and 
     * the value is the string value of the attribute.
     * 
     * the map returned by this method is live, and you can add new attribute
     * by updating the map directly. Because of this design, there's no setter.
     * 
     * 
     * @return
     *     always non-null
     */
    public Map<QName, String> getOtherAttributes() {
        return otherAttributes;
    }

}
