//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.06.26 at 01:49:24 PM PDT 
//


package org.scte.schemas._35;

import java.util.HashMap;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyAttribute;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.namespace.QName;


/**
 * See Section 10.3.3.1 - segmentation_upid()
 * 
 * <p>Java class for SegmentationUpidType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SegmentationUpidType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>token">
 *       &lt;attribute name="segmentationUpidType" type="{http://www.w3.org/2001/XMLSchema}unsignedByte" />
 *       &lt;attribute name="formatIdentifier" type="{http://www.w3.org/2001/XMLSchema}unsignedInt" />
 *       &lt;attribute name="segmentationUpidFormat">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;pattern value="text|hexbinary|base-64|private:.+"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *       &lt;anyAttribute/>
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SegmentationUpidType", propOrder = {
    "value"
})
public class SegmentationUpidType {

    @XmlValue
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String value;
    @XmlAttribute(name = "segmentationUpidType")
    @XmlSchemaType(name = "unsignedByte")
    protected Short segmentationUpidType;
    @XmlAttribute(name = "formatIdentifier")
    @XmlSchemaType(name = "unsignedInt")
    protected Long formatIdentifier;
    @XmlAttribute(name = "segmentationUpidFormat")
    protected String segmentationUpidFormat;
    @XmlAnyAttribute
    private Map<QName, String> otherAttributes = new HashMap<QName, String>();

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the segmentationUpidType property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getSegmentationUpidType() {
        return segmentationUpidType;
    }

    /**
     * Sets the value of the segmentationUpidType property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setSegmentationUpidType(Short value) {
        this.segmentationUpidType = value;
    }

    /**
     * Gets the value of the formatIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getFormatIdentifier() {
        return formatIdentifier;
    }

    /**
     * Sets the value of the formatIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setFormatIdentifier(Long value) {
        this.formatIdentifier = value;
    }

    /**
     * Gets the value of the segmentationUpidFormat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSegmentationUpidFormat() {
        return segmentationUpidFormat;
    }

    /**
     * Sets the value of the segmentationUpidFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSegmentationUpidFormat(String value) {
        this.segmentationUpidFormat = value;
    }

    /**
     * Gets a map that contains attributes that aren't bound to any typed property on this class.
     * 
     * <p>
     * the map is keyed by the name of the attribute and 
     * the value is the string value of the attribute.
     * 
     * the map returned by this method is live, and you can add new attribute
     * by updating the map directly. Because of this design, there's no setter.
     * 
     * 
     * @return
     *     always non-null
     */
    public Map<QName, String> getOtherAttributes() {
        return otherAttributes;
    }

}
